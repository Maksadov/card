<?php 
  include("modal.php")
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Товары</title>
    <link rel="stylesheet" href="assets/style.css">
</head>
<body>
  <main>
    <section class="card-list">
      <?php 
        $data = file_get_contents ("product.json"); // Выбор данных из JSON файла
        $json = json_decode($data, true); 
        foreach($json as $list){
          foreach($list as $item){
            ?>
              <div class="card">
                <img src="<?php echo $item['img'] ?>?>" class="card-image" alt="item-img">
                <div class="container">
                  <p><?php echo $item['name'] ?></p> 
                  <p><b><?php echo $item['price'] ?></b></p> 
                  <button class="card-button" onclick="document.getElementById('id01').style.display='block'">Купить</button>
                </div>
              </div> 
            <?php
          } 
        }
      ?>
    </section>
  </main>
  <script src="assets/script.js"></script>
</body>
</html>